<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function ok(){
        return view('welcome');
    }

    public function error(){
        $test = User::test;
//        return $test;
//        return json_encode(collect($test));
        return view('hello',[
           'error' => $test
        ]);
    }
}
