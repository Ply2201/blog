<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('home');
})->name('home');



Route::get('/about', function(){
    return view('about');
})->name('about');

Route::get('/background', function(){
    return 'This page will be about my background';
})->name('background');

Route::get('/education', function(){
    return 'This page will be about my past education';
})->name('education');

Route::get('/ambitions/{id}', function($id){
    return 'This page will be about my goals'.$id;
})->name('ambitions');



Route::get('/activities', function(){
    return view('activities');
})->name('activities');

Route::get('/sports', function(){
    return 'this page will be my sports history';
})->name('asdasd');

Route::get('/academic', function(){
    return 'this page will be about my academic background';
})->name('sadasdsdas');

Route::get('/charity', function(){
    return 'this page will be about charity work I did';
})->name('asdasdsd');



