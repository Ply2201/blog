<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    const test = [
        [
            'test1' => 'hello1',
            'test2' => '222',
            'test3' => '222',
            'test4' => '222',
            'test5' => '222',
        ],[
            'test1' => 'hello2',
            'test2' => '222',
            'test3' => '222',
            'test4' => '222',
            'test5' => '222',
        ]
    ];
}
